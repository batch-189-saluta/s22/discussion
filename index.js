// alert('hello world')

// ARRAY METHODS

/*
	1. Mutator methods 
		- are functions that mutate or change an array after they are created. These methods manipulate original array performing various tasks such as adding and removing elements

*/

let fruits = ["apple", "Orange", "Kiwi", "Dragon Fruit"];

/*
	push ()
		add an element in the end of an array AND returns the ARRAY'S LENGTH!

	syntax:
		arrayName.push(element)
*/

console.log("Current Array")
console.log(fruits)

// Adding elements
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength)
console.log("mutated array from push method")
console.log(fruits)

fruits.push("Avocado", "Guava");
console.log(fruits)

/*
	pop ()
		-removes the last element in our array AND returns the REMOVED ELEMENT!

	Syntax:
		arrayName.pop()
*/

let removedFruit = fruits.pop()
console.log(removedFruit)
console.log("Mutated array from the pop method")
console.log(fruits)


/*
	unshift()
		adds one or more elements at the beginning of an array

	syntax:
		arrayName.unshift(elementA)
		arrayName.unshift(elementA, ElementB)

*/

// let unshiftMethod = fruits.unshift("lime", "Banana")
// console.log(unshiftMethod)
fruits.unshift("lime", "Banana")
console.log("Mutated array from unshift method: ")
console.log(fruits)

/*
	shift()
		remove an element at the BEGINNING of our array AND returns the REMOVE ELEMENT

	syntax:
		arrayName.shift()
*/

let anotherFruit = fruits.shift()
console.log(anotherFruit)
console.log("mutated array from the shift method")
console.log(fruits)

/*
	splice()
		simultaneously removes elements from a specified index number and adds an element

	Syntax:
		arrayName.splice(starting index, deleteCount, elementsToBeAdded)

*/

fruits.splice(1, 2, "lime", "cherry")
// pag mag aadd sa gitna, fruit.splice(start index, 0, element)

let fruitsSplice = fruits.splice(1, 2, "lime", "cherry")
console.log(fruitsSplice)
console.log("mutated array from splice method")
console.log(fruits)

console.log(fruits[0])
/*
	sort()
		rearranges the array elements in alphanumeric order (ASCENDING ORDER)

	Syntax:
		arrayName.sort();


*/
// magbabago din ang index number
fruits.sort()
console.log("mutated array from sort method")
console.log(fruits)
console.log(fruits[0])

let mixedArray = [50, 14, 100, "Carlos", "Nej", "Bernard", "Charles"]
// pag numerical sa character pa din nag babase hindi sa value
console.log(mixedArray.sort())



/*
	reverse()
		reverses the order of  array elements

	syntax:
		arrayName.reverse()
*/
// nirereverse lang yung position ng elements. hindi siya nag sosort
fruits.reverse()
console.log("mutated array from the reverse method")
console.log(fruits)

/*
	for sorting the items in descending order
	fruits.sort().reverse()
*/

// -----------------------------------------------------

/*
	Non-mutator methods
		-these are functions that do not modify or change an array after they are created. these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing the output.

*/

let countries = ["US", "PH", "CA", "SG", "TH", "PH", "FR", "DE"];
/*
	indexOf()
		returns the index number of the first matching element found in an array. if no match was found, the result will be -1. The search process will be done from first element proceeding to the last element

	syntax:
		arrayName.indexOf(searchValue)
		arrayName.indexOf(searchValue, fromIndex)

*/

let firstIndex = countries.indexOf("PH", 0);
// let firstIndex1 = countries.indexOf("PH", -1);
console.log("result of indexOf method: "+ firstIndex)
// console.log(firstIndex1)


/*
	lastIndexOf()
		-returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first element.

	syntax:
		arrayName.lastIndexOf(searchValue)
		arrayName.lastIndexOf(searchValue, fromIndex)
	
*/

let lastIndex = countries.lastIndexOf("PH")
console.log("result of lastIndexOf: "+lastIndex)

let lastIndexStart = countries.lastIndexOf("PH", 4)
console.log("result of lastIndexOf: "+lastIndexStart)



/*
	slice()
		-portions/slices elements from an array AND it returs NEW ARRAY

	syntax:
		arrayName.slice(startIndex)
		arrayName.slice(startingIndex, endingIndex) 

		//ending index is not included

*/
console.log(countries)
let slicedArrayA = countries.slice(2)
console.log("the result from slice method: ")
console.log(slicedArrayA)

let slicedArrayB = countries.slice(2,4)
console.log("result from slice method:")
console.log(slicedArrayB)

let slicedArrayC = countries.slice(-3)
console.log("result from slice method:")
console.log(slicedArrayC)



/*
	toString()
		returns an array as a string separated by commas

	syntax:
		arrayName.toString()
*/

let stringArray = countries.toString()
console.log("result from toString method: ");
console.log(stringArray)

/*
	concat()
		combines two arrays and returns the combined result

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA)

*/

let taskArrayA = ["drink html", "eat javascript"]
let taskArrayB = ["inhale css", "breath sass"]
let taskArrayC = ["get git", "be node"]

let tasks = taskArrayA.concat(taskArrayB);
console.log("result from concat method: ");
console.log(tasks)

// COMBINING MULTIPLE ARRAYS
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log("result from concat method: ");
console.log(allTasks)

// COMBINING ARRAYS WITH ELEMENTS
let combinedTasks = taskArrayA.concat("smell express", "throw react")
console.log(combinedTasks)

/*
	join()
		returns an array as a string separator by specified separator string

	syntax:
		arrayName.join(separatorString)
	
*/

let students = ["tristan", "Bernard", "Carlos", "Nehemia"];
console.log(students.join());
console.log(students.join(' '));
console.log(students.join(" - "));



// --------------------------------------------------------





// ITERATION METHODS
/*
	iteration methods are loops designed to perform repetitive tasks on arrays.  useful for manipulating array data resulting in complex tasks

*/

/*
	forEach()
		similar to for loop that iterated on each array element

	syntax:
		arrayName.forEach(function(indivdualElement) {
			statement
		})

*/


allTasks.forEach(function(task) {
	console.log(task)
})

// using forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task) {
	if (task.length > 10) {
		filteredTasks.push(task)
	}
})

console.log("result of filteredTasks: ")
console.log(filteredTasks)


/*
		map()
			-iterates on each element AND returns new array with different values depending on the result of the function's operation

		syntax:
			let/const resultArray = arrayName.map(function(individualElement) {
				statement
			})

*/

let numbers = [1,2,3,4,5]

let numberMap = numbers.map(function(number) {
	console.log(number)
	return number * number
})

console.log("original Array:")
console.log(numbers)
console.log("result of map method")
console.log(numberMap)




/*
	every()
		checks if all elements in an array met the given condition
		returns a true value if all elements meet condition and false if otherwise

	syntax:
		let/const resultArray = arrayName.every(function(individualElement) {
			return expression/condition
		})
*/

let allValid = numbers.every(function(number) {
	return (number < 3)
})

console.log("result of every method");
console.log(allValid);



/*
	some()
		checks if atleast one element in the array meets the given condition. Returns a true value if at least one element meets the given condition and false if otherwise

	syntax:
		let/const resultArray = arrayName.some(function(individualElement) {
			return expression/condition
		})
*/

let someValid  = numbers.some(function(number) {
	return (number < 2)
})
console.log("result of some method");
console.log(someValid);




/*
	filter()
		returns new array that contains elements which meets the given condition. return an empty array if no elements were found.

	syntax:
		let/const resultArray = arrayName.filter(function(individualElement) {
			return expression/condition
		})
*/

let filterValid = numbers.filter(function(number) {
	return (number < 3)
})
console.log("result of filter method");
console.log(filterValid);

let nothingFound = numbers.filter(function(number) {
	return (number == 0)
})
console.log("result of another filter method");
console.log(nothingFound);



// filter using forEach

let filteredNumbers = [];

numbers.forEach(function(number) {
	if (number < 3) {
		filteredNumbers.push(number)
	}
})
console.log("result of filtering using forEach: ")
console.log(filteredNumbers)


// -------------------------------------------------------


let products = ["mouse", "keyboard", "laptop", "monitor"];
/*
	includes method
		-methods can be "chained" using them one after another. the result of the first method is used on the second method until all "chained" methods have been resolved.

*/

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes("a")
	// includes - lahat ng may letter  "a"
	// pag string, case sensitive kaya dapat naka toLowerCase
})
console.log(filteredProducts)